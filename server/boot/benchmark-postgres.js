module.exports = function (app) {

    var STATUSCOUNT = 100;

    var statusesArray = [];
    for (var i = 0; i < STATUSCOUNT; i++) {
        statusesArray.push({
            deviceId: 1,
            name: "status #" + String(i),
            type: "someStatusType",
            value: i
        });
    }

    var postgres = app.dataSources.postgres;
    postgres.automigrate('device')
        .then(function () {
            return postgres.automigrate("status");
        })
        .then(function () {
            console.log("Adding device");
            return app.models.device.create({
                name: "device1",
                type: "sometype"
            });
        })
        .then(function () {
            console.log("Added device. Creating statuses.");
            return app.models.status.create(statusesArray);
        })
        .then(function () {
            console.log("Benchmark 1: updating one status at a time");

            var upserts = [];
            for (var i = 0; i < STATUSCOUNT; i++) {
                var status = statusesArray[i];
                status.id = i + 1;
                status.value = i * 10;
                upserts.push(app.models.status.upsert(status));
            }

            console.time('b1');
            return Promise.all(upserts);
        })
        .then(function () {
            console.timeEnd('b1');
            console.log("Benchmark 2: bulkUpdate");

            var updates = [];
            for (var i = 0; i < STATUSCOUNT; i++) {
                var status = statusesArray[i];
                status.id = i + 1;
                status.value = i * 100;
                updates.push(status);
            }

            console.time('b2');
            //Attempt #1: [AssertionError: Cannot get a setup Change model for status]
            //return app.models.status.bulkUpdate(updates);
            
            //Attempt #2: [AssertionError: Cannot get a setup Change model for status]
            app.models.status.currentCheckpoint()
                .then(function (currentCheckpointId) {
                    return app.models.status.diff(currentCheckpointId, updates)
                })
                .then(function (deltas) {
                    return app.models.status.createUpdates(deltas)
                })
                .then(function (createdUpdates) {
                    return app.models.status.bulkUpdate(createdUpdates);
                })
        })
        .then(function () {
            console.timeEnd("b2");
        })
        .catch(function (err) {
            console.log(err);
        })
}