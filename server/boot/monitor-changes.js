var es = require('event-stream');
var process = require('process');

module.exports = function (app) {
    //var modelsToWatch = [app.models.device, app.models.status];
    var modelsToWatch = [];
    
    modelsToWatch.forEach(function (model) {
        model.createChangeStream(function (err, changes) {
            changes.pipe(es.stringify()).pipe(process.stdout);
        });
    })
}